<?php
function openTTD(){

    $ADDRESS = "games.sucs.org";
    $PORT = 3979;

    if (($sock = socket_create(AF_INET,SOCK_DGRAM,0)) === false){
        return ["online"=>false,"error"=>socket_strerror(socket_last_error($sock))];
    }

    if(socket_connect($sock,$ADDRESS,$PORT) === false){
        return ["online"=>false,"error"=>socket_strerror(socket_last_error($sock))];
    }

    socket_write($sock,"\x03\x00\x00",3);

    $out = socket_read($sock,2048);
    socket_close($sock);

    $length = unpack("v",substr($out,0,2))[0];
    $info1 = unpack("CCC",substr($out,2,3));

    $packetId = $info1[0];
    if ($packetId != 1){
        return ["online"=>false,"error"=>"server sent unexpected response"];
    }

    $protocolVersion = $info1[1];
    if ($protocolVersion != 4) {
        return ["online"=>false,"error"=>"server sent unsported packet version"];
    }

    $grfsCount = $info1[2];
    $data = substr($out,5);
    $grfs = readGrfs($data,$grfsCount);

    $info2 = unpack("VVCCC",substr($data,0,7));
    $gameDate = $info2[0];
    $startDate = $info2[1];
    $companiesMax = $info2[2];
    $companiesOn = $info2[3];
    $spectatorsMax = $info2[4];

    $partsT = explode("\x00",substr($data,7));

    $serverName = $partsT[0];
    $serverRevision = $partsT[1];

    $info3 = unpack("CCCCC",substr($partsT[2]));
    $serverLang = $info3[0];
    $passworded = $info3[1];
    $clientsMax = $info3[2];
    $clientsOn = $info3[3];
    $spectatorsOn = $info3[4];

    $mapName = $partsT[3];

    $info4 = unpack("vvCC",$partsT[4]);
    $mapWidth = $info4[0];
    $mapHeight = $info4[1];
    $mapSet = $info4[2];
    $dedicated = $info4[3];

    return [
        "online"=>true,
        "description"=>$serverName,
        "map"=>$mapName,
        "players"=>[
            "current"=>$clientsOn,
            "max"=>$clientsMax
        ]
    ];
}

function readGrfs(&$data,$number){
    $rtn = [];
    for($i=0;$i<number;$i++){
        $rtn[] = unpack("VB128",substr($data,i*130,130));
    }
    $data = substr($data,$number*130);
    return $rtn;
}

#echo json_encode(openTTD());
?>
