<?php
function minecraft(){

    $ADDRESS = "games";
    $PORT = 25565;

    if (($sock = socket_create(AF_INET,SOCK_STREAM,SOL_TCP)) === false){
        return ["online"=>false,"error"=>socket_strerror(socket_last_error($sock))];
    }

    if(socket_connect($sock,$ADDRESS,$PORT) === false){
        return ["online"=>false,"error"=>socket_strerror(socket_last_error($sock))];
    }

    $input = "\x0f\x00\x2f\x09127.0.0.1c\xdd\x01\x01\x00";
    socket_write($sock,$input,strlen($input));

    $out = socket_read($sock,2048);
    socket_close($sock);

    $t = readVarint($out);
    $out = $t["data"];
    $len = $t["res"];

    if ( substr($out,0,1) !== "\x00"){
        return ["online"=>false,"error"=>"server sent unexpected result"];
    }

    $t = readVarint(substr($out,1));
    $out = $t["data"];
    $len = $t["res"];

    $result = json_decode($out,$assoc=true);

    $players = [];

    if ($result["players"]["sample"]){
        foreach ($result["players"]["sample"] as $p){
            $players[] = $p["name"];
        }
    }

    return [
        "online"=>true,
        "description"=>$result["description"],
        "players"=>[
            "current"=>$result["players"]["online"],
            "max"=>$result["players"]["max"],
            "list"=>$players
        ],
        "version"=>$result["version"]["name"],
        "extra"=>"<a href='http://games.sucs.org/dynmap'>Live Map</a>"
    ];
}

function readVarint($data){
    $result = 0;
    $first = true;
    for($i=0;$i<strlen($data);$i++){
        $part = ord(substr($data,$i,1));
        $result |= ($part & 0x7F) << 7 * $i;
        if (($part & 0x80) == 0){
            break;
        }
    }
    return ["res"=>$result,"data"=>substr($data,$i+1)];
}

#echo json_encode(minecraft());
?>
