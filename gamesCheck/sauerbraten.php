<?php
function sauerbraten(){

    $ADDRESS = "games.sucs.org";
    $PORT = 28786;

    if (($sock = socket_create(AF_INET,SOCK_DGRAM,0)) === false){
        return ["online"=>false,"error"=>socket_strerror(socket_last_error($sock))];
    }

    if(socket_connect($sock,$ADDRESS,$PORT) === false){
        return ["online"=>false,"error"=>socket_strerror(socket_last_error($sock))];
    }

    $reqId = "GI\n";
    socket_write($sock,$reqId,3);

    $out = socket_read($sock,2048);
    socket_close($sock);

    if ( substr($out,0,3) !== $reqId){
        return ["online"=>false,"error"=>"server sent unexpected response"];
    }
    $out = substr($out,3);

    $numPlayers = readInt($out);
    $numAttrs = readInt($out);

    $protcolVersion = readInt($out);
    $gameMode = readInt($out);
    $timeLeft = readInt($out);
    $maxClients = readInt($out);
    $masterMode = readInt($out);

    if ($numAttrs == 7){
        $gamePaused = readInt($out);
        $gameSpeed = readInt($out);
    } else {
        $gamePaused = 0;
        $gameSpeed = 100;
    }
    $tmp = explode("\x00",$out);

    $mapName = $tmp[1];
    $serverDesc = $tmp[2];

    $nnn = [
        "Free for all",
        "Coop Edit",
        "Teamplay",
        "Instagib",
        "Instagib Team",
        "Efficiency",
        "Efficiency team",
        "Tactics",
        "Tactics team",
        "Capture",
        "Regen capture",
        "Capture the flag",
        "Insta Capture the flag",
        "Protect",
        "Insta Protect",
        "Hold",
        "Insta Hold",
        "Efficiency Capture the flag",
        "Efficiency Protect",
        "Efficiency Hold",
        "Collect",
        "Insta Collect",
        "Efficiency Collect"
    ];
    $gameMode = $nnn[$gameMode];

    return [
        "online"=>true,
        "description"=>$serverDesc,
        "map"=>$mapName,
        "players"=>["current"=>$numPlayers,"max"=>$maxClients],
        "gameMode"=>$gameMode
    ];
}

function readInt(&$out){
    $c = ord(substr($out,0,1));
    if ($c == -128){
        $out = substr($out,1);
        return unpack("v",substr($out,1,2));
    } elseif ($c == -127){
        $out = substr($out,1);
        return unpack("V",substr($out,1,4));
    } else {
        $out = substr($out,1);
        return $c;
    }
}

#echo json_encode(sauerbraten());
?>
