<?php

/*
* SUCS GameAuth v2
* Follows the idea of a SPA, largely based around php sessions
* User loads the page, session is started (or resumed), if it's in the db then
* the state is restored. Otherwise the see the page with a login form. POST
* requests to self with user/pass, if successfully authd using my LDAP auth
* lib then database is poked with their details.

* Idea mainly follows the old system, some things like bans were a last minute
* afterthought that's why they are so barebones
*/

// include my ldap auth lib
include('ldap-auth.php');
error_reporting(E_ERROR);

// star/resume a session
session_start();

// initialise some variables we'll use later
$authd; // if they get authd, how, otherwise "nope"
$authdUser; // once authd, this is their username
$sessionid = session_id();
$time = time();
$loginMessage = "<p>You are now logged into the SUCS Game Server system, and can connect to any of the servers we have running by simply specifying the hostname/IP address 'games.sucs.org'. This page must be left open while you are playing. When you close this window, you will no longer have access to the games server, and will have to login again if you wish to play some more.</p>";

$uniAllowFilePATH = '/home/game-server/uni.allow';
$gameauthDBPATH = 'gameauth.db';#'/tmp/gameauth.db'; Changed for local testing, do not commit to main site

// create the db object, if the db aint there then make it
if (!file_exists($gameauthDBPATH)){
	$db = new SQLite3($gameauthDBPATH);
	$db->exec("CREATE TABLE gamers
		(
    		username TEXT PRIMARY KEY NOT NULL,
    		sessionid TEXT NOT NULL,
    		IP TEXT NOT NULL,
    		type TEXT NOT NULL,
    		lastseen INT NOT NULL
		)"
	);
	$db->exec("CREATE TABLE bans
		(
    		username TEXT PRIMARY KEY NOT NULL,
    		reason TEXT
		)"
	);
} else {
	$db = new SQLite3($gameauthDBPATH);
}

$cip=$_SERVER['REMOTE_ADDR'];
$cip2=$_SERVER['HTTP_CLIENT_IP'];
$cip3=$_SERVER['HTTP_X_FORWARDED_FOR'];
$cookie=$_COOKIE["sucs_gameauth"];
$username=$_POST['username'];
$password=$_POST['password'];

/*echo("REMOTE_ADDR: $cip <br>");
echo("HTTP_CLIENT_IP: $cip2 <br>");
echo("HTTP_X_FORWARDED_FOR: $cip3 <br>");*/

// get a list of sessions in the db and banned users
$sessionsResult = $db->query("SELECT sessionid FROM gamers");
$bannedUsers = $db->query("SELECT username FROM bans");

// store sessions in another data format (1d array), easier to search
$sessions = array();
$i = 0;
while($res = $sessionsResult->fetchArray(SQLITE3_ASSOC)){
	if(!isset($res['sessionid'])) continue;
	$sessions[$i] = $res['sessionid'];
	$i++;
}

// if their session is in the known sessions list, then update the db with the
// current IP and time. otherwise look at the POST data and do stuff related
// to that, such as trying to auth etc...
if (in_array($sessionid, $sessions)){

	$query = $db->query("SELECT type FROM gamers WHERE sessionid='$sessionid'");
	$authd = $query->fetchArray()[0];

	$query = $db->query("SELECT username FROM gamers WHERE sessionid='$sessionid'");
	$authdUser = $query->fetchArray()[0];

	$db->exec("DELETE FROM gamers WHERE username='$authdUser'");
	$time = time();
	$db->exec("INSERT INTO gamers (username,sessionid,IP,type,lastseen) VALUES ('$authdUser','$sessionid','$cip','$authd','$time')");

}else {

	if ($_POST["username"] == "" && $_POST["password"] == "") {
		$authd = "";
	} else {
		// the main auth bit
		$authd = ldapAuth($username, $password);
		// bingo! we have a valid account
		if ($authd == "sucs" || $authd == "uni") {
			// people like to use emails to login so lets detect and strip
			if(filter_var($username, FILTER_VALIDATE_EMAIL)){
				//valid email, lets strip
				// split the email into a string array "@" as a delim
				$s = explode("@",$username);
				// remove the last element (domain)
				array_pop($s);
				// put the array back togther using "@" as a seperator
				$username = implode("@",$s);
			}
			$authdUser = strtolower($username);
			$type = $authd;
			// check if they are banned, display message if so
			// otherwise, happy days
			if (in_array($authdUser, $bannedUsers->fetchArray())) {
				$loginMessage = "<p>Sorry you are banned. For more information contact games@sucs.org</p>";
			} else {
				if ($authd == "sucs") {
					$type = "sucs";
					$db->exec("DELETE FROM gamers WHERE username='$authdUser'");
					$db->exec("INSERT INTO gamers (username,sessionid,IP,type,lastseen) VALUES ('$authdUser','$sessionid','$cip','$type','$time')");
				} elseif ($authd == "uni") {
					if (file_exists($uniAllowFilePATH)) {
						$type = "uni";
						$db->exec("DELETE FROM gamers WHERE username='$authdUser'");
						$db->exec("INSERT INTO gamers (username,sessionid,IP,type,lastseen) VALUES ('$authdUser','$sessionid','$cip','$type','$time')");
					} else {
						$loginMessage = "<p>Thank you for taking an interest in playing on the SUCS game server. Unfortunately the game server is currently only available to SUCS members, you can <a href=\"https://sucs.org/join\">sign up</a> to SUCS and get 24/7 access to the server plus all the other benefits that come with SUCS membership.</p>";
					}
				} else {
					// never get to this stage
				}
			}
		}else if ($authd == "nope"){
			//nothing special
			// could do some login attempts thing here???
		}
	}
}

// logouts are done by posting the username logout to the page
if ($_POST["username"] == "logout"){
	//$db->exec("DELETE FROM gamers WHERE sessionid='$sessionid'");
	$db->exec("UPDATE gamers SET lastseen=0, sessionid='n0p3' WHERE sessionid='$sessionid'");
	session_destroy();
	$authd = "";
}

//echo ("Auth'd?: $authd <br>");

//echo("Cookie: $cookie <br>");

//echo ("Session id: ");
//echo (session_id());

// the html, currently using bootstrap
echo("
	<!doctype html>
	<html lang=\"en\">
		<head>
");

if ($authd == "sucs" || $authd == "uni"){
	echo("<META HTTP-EQUIV=\"refresh\" CONTENT=\"30\">");echo("<META HTTP-EQUIV=\"refresh\" CONTENT=\"30\">");
}

echo("
	<meta http-equiv=\"Content-type\" content=\"text/html;charset=UTF-8\" />

			<link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">
		    <link href=\"css/ripples.min.css\" rel=\"stylesheet\">
    		<link href=\"css/material-wfont.min.css\" rel=\"stylesheet\">

			<title>SUCS Games Server</title>
		</head>

");

echo("
	<nav class=\"navbar navbar-inverse navbar-warning\">
		<div class=\"container\">
        	<div class=\"navbar-header\">
				<a class=\"navbar-brand\" rel=\"home\" href=\"\" title=\"SUCS\">
					<img style=\"height: 40px; margin-top: -5px;\" src=\"res/sucs_logo.png\">
    			</a>
        	</div>
      	</div>
    </nav>
");

echo("

		<div class=\"col-md-8\">
			<div class=\"panel panel-default\">
				<div class=\"panel-body\">
					<h3>SUCS Games Server</h3>
					<p style=\"font-size:1.0em\">
						Being a member of SUCS gives you exclusive access to our games server. If you're living on campus, it's the only way to play multiplayer online games - and it's also accessible to members living off campus, too. See below for a list of the games we currently offer.
						<br>
						<br>
						<h4>How do I play?</h4>
						Before you can connect to our games server, you need to log in to our authentication system. Simply use the login box on the right hand side of this page to log in to the system, using your SUCS username and password or your Student Number and password.
						<br>
						<br>
						Steam users are also welcome to join the <a href=\"http://steamcommunity.com/groups/swanseauniversitycompsoc\">SUCS group</a>
						<br>
						<br>
						<h4>Games Currently Available</h4>
						Click each game for more information, as well as links to download any addons or patches you may need.
					</p>
				</div>
			</div>
	    </div>

");

echo("

	<div class=\"col-md-4\">
		<div class=\"panel panel-default\">
			<div id=\"user-panel\" class=\"panel-body\">

");

if ($authd == "nope") {
	echo("

		<div class=\"alert alert-danger\" role=\"alert\">
			<span class=\"glyphicon glyphicon-exclamation-sign\" aria-hidden=\"true\"></span>
			<span class=\"sr-only\">Error:</span>
	  		You have entered invalid credentials.
		</div>

	");
}
if ($authd != "sucs" && $authd != "uni"){
	echo("

		<form method=\"post\" class=\"form-login\">
			<div class=\"form-group\">
				<input type=\"text\" name=\"username\" placeholder=\"Username\" class=\"form-control\">
			</div>
			<div class=\"form-group\">
				<input type=\"password\" name=\"password\" placeholder=\"Password\" class=\"form-control\">
			</div>
			<button type=\"submit\" class=\"btn btn-success\">Sign in</button>
			Login with your SUCS username or Student Number
		</form>
		<form action=\"https://sucs.org/join\" class=\"form-signup\">
			<input type=\"submit\" value=\"Sign up\" class=\"btn btn-warning\">
		</form>

	");
}else if ($authd == "sucs" || $authd == "uni") {
	echo("

		<div id=\"username\">
			<p>Hello $authdUser!</p>
		</div>

	");
	echo($loginMessage);
	echo("

		<div id=\"logout-bttn\">
			<form action=\"\" method=\"post\">
				<input type=\"hidden\" value=\"logout\" name=\"username\">
				<input type=\"submit\" value=\"Log out\" class=\"btn btn-danger\">
			</form>
		</div>

	");
	echo("

		</div>
		</div>
		</div>

	");

	$loggedInUsers = $db->query("SELECT username FROM gamers");

	echo("

		<div class=\"col-md-4\">
          <div class=\"panel panel-default\">
              <div id=\"user-panel\" class=\"panel-body\">

	");

	echo("<div class=\"online-users\">");
	echo("<p>List of people online: </p>");
	echo("<ul>");
	while ($row = $loggedInUsers->fetchArray()) {
		echo("<li> $row[0]");
	}
	echo("</ul>");
	echo("</div>");

}

echo("
		</div>
		</div>
		</div>
");

echo("
		<div class=\"container\">
			<div class=\"row\">
");

include "gamesCheck/minecraft.php";
include "gamesCheck/openTTD.php";
include "gamesCheck/sauerbraten.php";

$data = [
	"Minecraft" => minecraft(),
	"OpenTTD" => openTTD(),
	"Sauerbraten" => sauerbraten()];
foreach ($data as $name=>$result){
	if ($result["extra"]){
		$extra = "<p>".$result["extra"]."</p>";
	} else {
		$extra = "";
	}
	if ($result["online"]){
		$description = $result["description"];
		$playersOn = $result["players"]["current"];
		$playersMax = $result["players"]["max"];
		echo("
			<div class=\"col-md-4\">
				<div class=\"panel panel-default\">
					<div class=\"panel-body\">
						<h2>$name - <font color=\"green\">Online</font></h2>
						<p>$description</p>
						<p>$playersOn/$playersMax Players Online</p>
						$extra
					</div>
				</div>
			</div>
		");
	} else {
		echo("
			<div class=\"col-md-4\">
				<div class=\"panel panel-default\">
					<div class=\"panel-body\">
						<h2>$name - <font color=\"red\">Offline</font></h2>
						<p>${result["error"]}</p>
						$extra
					</div>
				</div>
			</div>
		");
	}
}

echo("

	<div id=\"footer\">
		<div class=\"container text-center\">
			<p class=\"text-muted credit\" style=\"color:#fff\"><p>Site designed and maintained by SUCS. Please email games@sucs.org if you have any problems with this service.</p>
		</div>
	</div>

");

echo("</html>");

?>
