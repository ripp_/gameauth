#!/bin/bash

start(){
	/sbin/iptables -F INPUT
	/sbin/iptables -P INPUT DROP
	/sbin/iptables -A INPUT -i lo -j ACCEPT
	/sbin/iptables -A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT
	/sbin/iptables -A INPUT -s 137.44.10.0/24 -j ACCEPT
	/sbin/iptables -A INPUT -p TCP --dport 80 -j ACCEPT
	/sbin/iptables -A INPUT -p TCP --dport 443 -j ACCEPT
	/usr/local/src/gameauth/empty_users_table.py
}
stop(){
	/sbin/iptables -F INPUT
	/sbin/iptables -P INPUT ACCEPT
	/usr/local/src/gameauth/empty_users_table.py
}

case "$1" in
    start)
          start
	  ;;
    stop)
          stop
	  ;;
    *)
    	  echo "Usage: $0 {start|stop}"
	  exit 1
esac
